.DEFAULT_GOAL := help

.PHONY: dep
dep: ## Install the dependencies
	python3 -m pip install -r requirements.txt

.PHONY: db-setup
db-setup: ## Initialize the database and seed the database
	FLASK_APP=src FLASK_ENV=development flask db-init
	FLASK_APP=src FLASK_ENV=development flask user-create admin --password secret

.PHONY: run
run: ## Run the application binary
	FLASK_APP=src FLASK_ENV=development flask run

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
