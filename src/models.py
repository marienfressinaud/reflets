import sqlite3
import werkzeug.security as security

from . import db


class User:
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def check_password(self, password):
        return security.check_password_hash(self.password, password)

    @classmethod
    def find_by_id(cls, user_id):
        database = db.get_db()
        user = database.execute(
            "SELECT * FROM users WHERE id = ?", (user_id,)
        ).fetchone()

        if user is None:
            return None

        return cls(user["id"], user["username"], user["password"])

    @classmethod
    def find_by_username(cls, username):
        database = db.get_db()
        user = database.execute(
            "SELECT * FROM users WHERE username = ?", (username,)
        ).fetchone()

        if user is None:
            return None

        return cls(user["id"], user["username"], user["password"])

    @classmethod
    def create(cls, username, password):
        database = db.get_db()
        cursor = database.execute(
            "INSERT INTO users (username, password) VALUES (?, ?)",
            (username, security.generate_password_hash(password)),
        )
        database.commit()

        return cls.find_by_id(cursor.lastrowid)


class Event:
    def __init__(self, id, title, date, date_end, is_private, project_id, description):
        self.id = id
        self.title = title
        self.date = date
        self.date_end = date_end
        self.is_private = is_private
        self.project_id = project_id
        self.description = description

        # yep, that's pretty inefficient... MR are opened :)
        self.project = Project.find_by_id(self.project_id)

    @classmethod
    def items(cls, **kwargs):
        attributes = kwargs.keys()
        values = kwargs.values()
        where = []
        for attribute in attributes:
            where.append("{0} = :{0}".format(attribute))

        database = db.get_db()

        sql = "SELECT * FROM events"
        if where:
            sql = sql + " WHERE {}".format(" AND ".join(where))

        cursor = database.execute(sql, tuple(values))
        event = cursor.fetchone()
        while event:
            yield cls(
                event["id"],
                event["title"],
                event["date"],
                event["date_end"],
                event["is_private"],
                event["project_id"],
                event["description"],
            )

            event = cursor.fetchone()

    @classmethod
    def find_by_id(cls, event_id):
        database = db.get_db()
        event = database.execute(
            "SELECT * FROM events WHERE id = ?", (event_id,)
        ).fetchone()

        if event is None:
            return None

        return cls(
            event["id"],
            event["title"],
            event["date"],
            event["date_end"],
            event["is_private"],
            event["project_id"],
            event["description"],
        )

    @classmethod
    def create(cls, title, date, date_end, is_private, project_id, description):
        database = db.get_db()
        cursor = database.execute(
            (
                "INSERT INTO events "
                "(title, date, date_end, is_private, project_id, description) "
                "VALUES (?, ?, ?, ?, ?, ?)"
            ),
            (title, date, date_end, is_private, project_id, description),
        )
        database.commit()

        return cls.find_by_id(cursor.lastrowid)

    def save(self):
        try:
            database = db.get_db()
            database.execute(
                (
                    "UPDATE events "
                    "SET title = ?, date = ?, date_end = ?, is_private = ?, "
                    "    project_id = ?, description = ? "
                    "WHERE id = ?"
                ),
                (
                    self.title,
                    self.date,
                    self.date_end,
                    self.is_private,
                    self.project_id,
                    self.description,
                    self.id,
                ),
            )
            database.commit()
            return True
        except sqlite3.DatabaseError:
            return False

    def delete(self):
        try:
            database = db.get_db()
            database.execute("DELETE FROM events WHERE id = ?", (self.id,))
            database.commit()
            return True
        except sqlite3.DatabaseError:
            return False


class Project:
    def __init__(self, id, name, is_private, responsible, description):
        self.id = id
        self.name = name
        self.is_private = is_private
        self.responsible = responsible
        self.description = description

    def events(self, with_private=False):
        if with_private:
            return Event.items(project_id=self.id)
        else:
            return Event.items(project_id=self.id, is_private=False)

    @classmethod
    def items(cls, **kwargs):
        attributes = kwargs.keys()
        values = kwargs.values()
        where = []
        for attribute in attributes:
            where.append("{0} = :{0}".format(attribute))

        database = db.get_db()

        sql = "SELECT * FROM projects"
        if where:
            sql = sql + " WHERE {}".format(" AND ".join(where))

        cursor = database.execute(sql, tuple(values))
        project = cursor.fetchone()
        while project:
            yield cls(
                project["id"],
                project["name"],
                project["is_private"],
                project["responsible"],
                project["description"],
            )

            project = cursor.fetchone()

    @classmethod
    def find_by_id(cls, project_id):
        database = db.get_db()
        project = database.execute(
            "SELECT * FROM projects WHERE id = ?", (project_id,)
        ).fetchone()

        if project is None:
            return None

        return cls(
            project["id"],
            project["name"],
            project["is_private"],
            project["responsible"],
            project["description"],
        )

    @classmethod
    def find_by_name(cls, name):
        database = db.get_db()
        project = database.execute(
            "SELECT * FROM projects WHERE name = ?", (name,)
        ).fetchone()

        if project is None:
            return None

        return cls(
            project["id"],
            project["name"],
            project["is_private"],
            project["responsible"],
            project["description"],
        )

    @classmethod
    def create(cls, name, is_private, responsible, description):
        database = db.get_db()
        cursor = database.execute(
            (
                "INSERT INTO projects (name, is_private, responsible, description) "
                "VALUES (?, ?, ?, ?)"
            ),
            (name, is_private, responsible, description),
        )
        database.commit()

        return cls.find_by_id(cursor.lastrowid)

    def save(self):
        try:
            database = db.get_db()
            database.execute(
                (
                    "UPDATE projects "
                    "SET name = ?, is_private = ?, responsible = ?, description = ? "
                    "WHERE id = ?"
                ),
                (
                    self.name,
                    self.is_private,
                    self.responsible,
                    self.description,
                    self.id,
                ),
            )
            database.commit()
            return True
        except sqlite3.DatabaseError:
            return False

    def delete(self):
        try:
            database = db.get_db()
            database.execute("DELETE FROM projects WHERE id = ?", (self.id,))
            database.commit()
            return True
        except sqlite3.DatabaseError:
            return False
