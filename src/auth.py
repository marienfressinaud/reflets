import functools

from flask import (
    Blueprint,
    request,
    session,
    redirect,
    url_for,
    flash,
    render_template,
    g,
)

from .ldap_auth import Ldap, LdapException
from .models import User


bp = Blueprint("auth", __name__, url_prefix="/auth")


def login_required(view):
    """A decorator to require user to be logged in on certain views."""

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.current_user is None:
            return redirect(url_for("auth.login"))
        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    """Set the global current_user if logged in."""
    user_id = session.get("user_id")
    g.current_user = User.find_by_id(user_id)


@bp.route("/login", methods=("GET", "POST"))
def login():
    ldap = Ldap()
    ldap_error = False

    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        user = User.find_by_username(username)
        logged_in = False
        if not ldap.configured:
            logged_in = user is not None and user.check_password(password)
        else:
            try:
                logged_in = ldap.verify(username, password)
            except LdapException as e:
                ldap_error = str(e)

            if logged_in and not user:
                # Just create the user in DB so we can reference it by its
                # username later. The password is set only to be sure that one
                # cannot login with no password if ldap is deactivated.
                user = User.create(username=username, password=password)

        if logged_in:
            session.clear()
            session["user_id"] = user.id
            flash("Vous êtes désormais connecté·e.", "success")
            return redirect(url_for("index"))
        elif ldap_error:
            flash(
                "Un problème est survenu lors de la connexion LDAP : {}.".format(
                    ldap_error
                )
            )
        else:
            flash("Les identifiants sont incorrects.")

    return render_template("auth/login.html", ldap_configured=ldap.configured)


@bp.route("/logout")
def logout():
    session.clear()
    flash("Vous êtes désormais déconnecté·e.", "success")
    return redirect(url_for("index"))
