import ldap

from flask import current_app


class LdapException(RuntimeError):
    def __init__(self, ldap_error):
        message = ldap_error.args[0]
        if "desc" in message:
            message = message["desc"]

        self.message = message

    def __str__(self):
        return self.message


class Ldap:
    def __init__(self):
        self.auth_server = current_app.config["LDAP_AUTH_SERVER"]
        self.dn = current_app.config["LDAP_DN"]
        self.username = current_app.config["LDAP_USERNAME"]
        self.password = current_app.config["LDAP_PASSWORD"]

        self.configured = self.auth_server != ""

    def verify(self, username, password):
        try:
            connection = ldap.initialize(self.auth_server)
            connection.simple_bind_s(self.username, self.password)

            user = connection.search_s(
                self.dn, ldap.SCOPE_SUBTREE, "(uid={})".format(username)
            )
            if not user:
                return False

            user_dn = user[0][0]
            connection.bind_s(user_dn, password)
            connection.unbind_s()
            return True
        except ldap.INVALID_CREDENTIALS:
            return False
        except ldap.LDAPError as e:
            raise LdapException(e)
