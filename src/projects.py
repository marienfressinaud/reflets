from flask import (
    Blueprint,
    request,
    redirect,
    url_for,
    flash,
    render_template,
    g,
    abort,
)

from .auth import login_required
from .models import Project


bp = Blueprint("projects", __name__, url_prefix="/projects")


@bp.route("/")
def index():
    if g.current_user is None:
        projects = Project.items(is_private=False)
    else:
        projects = Project.items()

    return render_template("projects/index.html", projects=projects)


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    if request.method == "POST":
        name = request.form["name"].strip()
        is_private = request.form["is_private"].strip() == "yes"
        responsible = request.form["responsible"].strip()
        description = request.form["description"].strip()

        if not name:
            flash("Le nom est requis.")
        elif not Project.create(name, is_private, responsible, description):
            flash("Le projet n’a pas pu être ajouté.")
        else:
            flash("Le projet a été ajouté.", "success")
            return redirect(url_for("projects.index"))

    return render_template("projects/create.html")


@bp.route("/update/<project_id>", methods=("GET", "POST"))
@login_required
def update(project_id):
    project = Project.find_by_id(project_id)
    if not project:
        abort(404)

    if request.method == "POST":
        project.name = request.form["name"].strip()
        project.is_private = request.form["is_private"].strip() == "yes"
        project.responsible = request.form["responsible"].strip()
        project.description = request.form["description"].strip()

        if not project.name:
            flash("Le nom est requis.")
        elif not project.save():
            flash("Le projet n’a pas pu être modifié.")
        else:
            flash("Le projet a été modifié.", "success")
            return redirect(url_for("projects.index"))

    return render_template("projects/update.html", project=project)


@bp.route("/delete/<project_id>", methods=["DELETE"])
@login_required
def delete(project_id):
    project = Project.find_by_id(project_id)
    if not project:
        flash("Le projet n’existe pas.")
    elif not project.delete():
        flash("Le projet n’a pas pu être supprimé.")
    else:
        flash("Le projet a été correctement supprimé.", "success")

    return redirect(url_for("projects.index"))
