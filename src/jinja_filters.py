import re
import unicodedata
from datetime import datetime

from flask import Markup

import bleach
import markdown


ALLOWED_TAGS = [
    "ul",
    "ol",
    "li",
    "p",
    "pre",
    "code",
    "blockquote",
    "br",
    "strong",
    "em",
    "a",
]


def to_month_filter(number):
    """Convert a number to its equivalent month string."""
    return datetime.strptime(str(number), "%m").strftime("%B")


_markdown_instance = None


def markdown_filter(text):
    global _markdown_instance
    if _markdown_instance is None:
        _markdown_instance = markdown.Markdown(extensions=["nl2br"])

    unsafe_html = _markdown_instance.convert(text)
    sanitized_html = bleach.clean(unsafe_html, tags=ALLOWED_TAGS)
    return Markup(sanitized_html)


_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')


def slugify_filter(text):
    # Reference: http://flask.pocoo.org/snippets/5/
    result = []
    for word in _punct_re.split(text.lower()):
        word = unicodedata.normalize("NFKD", word).encode("ascii", "ignore")
        if word:
            result.append(word.decode("utf8"))
    return "-".join(result)


def init_app(app):
    """Register filters to the application Jinja."""
    app.jinja_env.filters["to_month"] = to_month_filter
    app.jinja_env.filters["markdown"] = markdown_filter
    app.jinja_env.filters["slugify"] = slugify_filter
