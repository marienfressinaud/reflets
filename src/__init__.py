import os
import locale

from flask import Flask

from . import jinja_filters, cli, db, auth, events, projects


__version__ = "0.1.0"


locale.setlocale(locale.LC_ALL, "")


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    database_default_path = os.path.join(app.instance_path, "db.sqlite")
    app.config.from_mapping(
        SECRET_KEY=os.environ.get("REFLETS_SECRET_KEY", "a-dev-secret"),
        DATABASE=os.environ.get("REFLETS_DATABASE", database_default_path),
        LDAP_AUTH_SERVER=os.environ.get("REFLETS_LDAP_AUTH_SERVER", ""),
        LDAP_DN=os.environ.get("REFLETS_LDAP_DN", ""),
        LDAP_USERNAME=os.environ.get("REFLETS_LDAP_USERNAME", ""),
        LDAP_PASSWORD=os.environ.get("REFLETS_LDAP_PASSWORD", ""),
    )

    try:
        os.makedirs(app.instance_path)
    except FileExistsError:
        pass

    # Register commands and teardowns to the app.
    jinja_filters.init_app(app)
    cli.init_app(app)
    db.init_app(app)

    # Register blueprints (collection of views)
    app.register_blueprint(auth.bp)
    app.register_blueprint(events.bp)
    app.register_blueprint(projects.bp)

    # Define the default route to the events index page
    app.add_url_rule("/", "index", events.index)

    return app
